// this is needed to post action to the app
(function() {
  window.postToRN = function(type, data) {
    const message = { type };
    if (data) message.payload = data;
    window.postMessage(JSON.stringify(message), "*");
  };
})();
// this func can be used to send auth credentials to b/e
// it is called after the html loaded
function sendToken(user, token) {
  // send the credentials from the app to the b/e
}
function init() {
  const button_skip = document.getElementById("skip");
  const button_data = document.getElementById("data");
  button_skip.onclick = function() {
    window.postToRN("SKIP");
  };

  // sending data to the app as an action object with
  // type [string] - action type
  // payload [object] - any data for that action
  // right now there's only one type declared - "SKIP"
  // this navigates to the app and closes the message
  button_data.onclick = function() {
    window.postToRN("DATA", {
      some_field: "Some data string",
      some_flag: true
    });
  };
}

window.addEventListener("DOMContentLoaded", init);
